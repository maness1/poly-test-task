package io.polybius.testtask;

import java.util.List;

public class Condition {
    List<String> keys;
    List<String> values;
    List<Integer> operations;

    Condition(List<String> keys, List<String> values, List<Integer> operations){
        this.keys = keys;
        this.values = values;
        this.operations = operations;
    }
}
