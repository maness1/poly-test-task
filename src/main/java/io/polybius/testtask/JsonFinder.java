package io.polybius.testtask;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;


public class JsonFinder {
    private String[] listOfOperators = new String[]{">=", "<=", "=", ">", "<",};
    private List<JsonObject> jsonObjectsList;
    private String query;



    JsonFinder(String query, String dataString){
        List<JsonObject> jsonObjectsList = new ArrayList<>();

        JsonParser jsonParser = new JsonParser();
        JsonArray jsonArray = jsonParser.parse(dataString).getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            jsonObjectsList.add(jsonArray.get(i).getAsJsonObject());
        }
        this.jsonObjectsList = jsonObjectsList;
        this.query = query.replace(" ","");
    }

    String checkForPresence() {
        List<JsonObject> filteredResult = new ArrayList<>();
        String[] orQueries = query.split("\\|\\|");  // First, split by ||. At least one condition must be true

        List<Condition> orConditions = new ArrayList<>();

        List<String[]> andQueries = new ArrayList<>();

        for (int i=0; i<orQueries.length; i++) {
            // a || b || c
            andQueries.add(orQueries[i].split("&&"));  // Split by &&. All splitted conditions must be true
            List<String> keys = new ArrayList<>();
            List<String> values = new ArrayList<>();
            List<Integer> operations = new ArrayList<>();

            for (int u = 0; u < andQueries.get(i).length; u++) {
                int actionId = returnOperationId(andQueries.get(i)[u]);
                if (actionId != -1) {
                    String key = andQueries.get(i)[u].split(listOfOperators[actionId])[0];
                    String value = andQueries.get(i)[u].split(listOfOperators[actionId])[1];
                    operations.add(actionId);
                    keys.add(key);
                    values.add(value);

                }
            }
            orConditions.add(new Condition(keys,values,operations));
        }


        for (JsonObject object : jsonObjectsList) {
            int fulfilledConditions = 0;
            for (int i = 0; i < orConditions.size(); i++) {
                for (int u = 0; u < orConditions.get(i).keys.size(); u++) {
                    if (checkStatement(object, orConditions.get(i).keys.get(u), orConditions.get(i).values.get(u), orConditions.get(i).operations.get(u))) {
                        fulfilledConditions++;
                    } else {
                        break;
                    }
                }
                if (fulfilledConditions == orConditions.get(i).keys.size()) {  // If all the conditions are true
                    filteredResult.add(object);
                    break;
                }
            }
        }
        return new Gson().toJson(filteredResult);
    }

    boolean checkStatement(JsonObject object, String key, String value, int actionId) {
        if(object.has(key)) {
            switch (actionId) {
                case 0: // Bigger or equal
                    if (object.get(key).getAsDouble() >= Double.parseDouble(value)) {
                        return true;
                    }
                    break;
                case 1: // Bigger or equal
                    if (object.get(key).getAsDouble() <= Double.parseDouble(value)) {
                        return true;
                    }
                    break;
                case 2: // Equality
                    if (value.matches("\\d+")) { // If dealing with numbers
                        if (object.get(key).getAsDouble() == Double.parseDouble(value)) {
                            return true;
                        }
                    } else { // If dealing with text
                        String str = object.get(key).getAsString().toLowerCase();

                        if (str.contains(value.toLowerCase())) {
                            return true;
                        }
                    }
                    break;
                case 3: // Bigger than
                    if (object.get(key).getAsDouble() > Double.parseDouble(value)) {
                        return true;
                    }
                    break;
                case 4: // Less than
                    if (object.get(key).getAsDouble() < Double.parseDouble(value)) {
                        return true;
                    }
                    break;

            }
        }
        return false;
    }

    int returnOperationId(String str) {  // Return the operation ID to proceed with checkStatement()
        for (int i=0; i<listOfOperators.length; i++) {
            if (str.contains(listOfOperators[i])) {
                return i;
            }
        }
        return -1;
    }
}
