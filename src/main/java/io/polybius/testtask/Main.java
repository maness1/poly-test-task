package io.polybius.testtask;

public class Main {



  public static void main(String[] args) throws Exception {
    String query = args[0];
    String dataString = args[1];

    String result = new JsonFinder(query, dataString).checkForPresence();

    System.out.println(result);
  }
}
