package io.polybius.testtask;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

class JsonFinderTest {

    @Test
    public void simpleStringTest() {

        String[] arg = new String[]{"name=Rob", "[{\"name\":\"Rob\"},{\"name\":\"Udo\"}]"};
        String output = "[{\"name\":\"Rob\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

    }
    @Test
    public void logicStringTest() {

        String[] arg = new String[]{"name=Bob", "[{\"name\":\"Bob\"},{\"name\":\"Bobby\"}]"};
        String output = "[{\"name\":\"Bob\"},{\"name\":\"Bobby\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

    }
    @Test
    public void logicStringTestWithNoOccurences() {

        String[] arg = new String[]{"name=Rob", "[{\"name\":\"Bob\"},{\"name\":\"Bobby\"}]"};
        String output = "[]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

    }
    @Test
    public void simpleEqualityTest() {

        String[] arg = new String[]{"age=21", "[{\"name\":\"Bob\",\"age\":\"25\"},{\"name\":\"Bobby\",\"age\":\"21\"}]"};
        String output = "[{\"name\":\"Bobby\",\"age\":\"21\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

    }
    @Test
    public void manyEqualitiesTest() {

        String[] arg = new String[]{"age=21", "[{\"name\":\"Bob\",\"age\":\"25\"},{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output = "[{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"},{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

    }
    @Test
    public void moreOrLessTest() {

        String[] arg = new String[]{"age>25", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output = "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"28\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

        String[] arg1 = new String[]{"age<25", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"22\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output1 = "[{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"},{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"22\"}]";
        JsonFinder finder1 = new JsonFinder(arg1[0], arg1[1]);
        assertEquals(finder1.checkForPresence(), output1);
    }

    @Test
    public void moreEqualOrLessEqualTest() {

        String[] arg = new String[]{"age>=25", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"25\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output = "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"25\"},{\"name\":\"Bobby\",\"age\":\"28\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

        String[] arg1 = new String[]{"age<=22", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"22\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output1 = "[{\"name\":\"Bobby\",\"age\":\"21\"},{\"name\":\"Bob\",\"age\":\"21\"},{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"22\"}]";
        JsonFinder finder1 = new JsonFinder(arg1[0], arg1[1]);
        assertEquals(finder1.checkForPresence(), output1);
    }

    @Test
    public void logicalAndConditionTest() {

        String[] arg = new String[]{"age>=25 && name=Bob", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"25\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output = "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"25\"},{\"name\":\"Bobby\",\"age\":\"28\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }

    @Test
    public void logicalOrConditionTest() {

        String[] arg = new String[]{"age>30 || name=Ub", "[{\"name\":\"Bob\",\"age\":\"26\"},{\"name\":\"Bobby\",\"age\":\"45\"},{\"name\":\"Bob\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"21\"},{\"name\":\"Bobby\",\"age\":\"28\"}]"};
        String output = "[{\"name\":\"Bobby\",\"age\":\"45\"},{\"name\":\"Ub\",\"age\":\"21\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }

    @Test
    public void logicalAndOrConditionTest() {

        String[] arg = new String[]{"age>30 || name=Ub && age<40", "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"},{\"name\":\"Ubo\",\"age\":\"21\"}," +
                "{\"name\":\"Ber\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"45\"},{\"name\":\"Bobby\",\"age\":\"55\"}]"};
        String output = "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"},{\"name\":\"Ubo\",\"age\":\"21\"},{\"name\":\"Ub\",\"age\":\"45\"},{\"name\":\"Bobby\",\"age\":\"55\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }

    @Test
    public void logicalAndOrConditionRepeatingResultTest() {

        String[] arg = new String[]{"age>30 || name=Ub", "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"},{\"name\":\"Oscar\",\"age\":\"21\"}]"};
        String output = "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }


    @Test
    public void fullArrayOnEmptyQuery() {

        String[] arg = new String[]{"", "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"},{\"name\":\"Oscar\",\"age\":\"21\"}]"};
        String output = "[{\"name\":\"Ub\",\"age\":\"40\"},{\"name\":\"Bob\",\"age\":\"35\"},{\"name\":\"Oscar\",\"age\":\"21\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }

    @Test
    public void decimalsInGreaterLessCondition() {

        String[] arg = new String[]{"height>50.5", "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output = "[{\"name\":\"Oscar\",\"height\":\"51\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

        String[] arg1 = new String[]{"height<50.5", "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output1 = "[{\"name\":\"Ub\",\"height\":\"50.0\"}]";
        JsonFinder finder1 = new JsonFinder(arg1[0], arg1[1]);
        assertEquals(finder1.checkForPresence(), output1);
    }

    @Test
    public void decimalsInGreaterEqualLessEqualCondition() {

        String[] arg = new String[]{"height>=50.5", "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output = "[{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);

        String[] arg1 = new String[]{"height<=50.5", "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output1 = "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"}]";
        JsonFinder finder1 = new JsonFinder(arg1[0], arg1[1]);
        assertEquals(finder1.checkForPresence(), output1);
    }

    @Test
    public void spacesInQuery() {

        String[] arg = new String[]{"height >= 50.5 && name=Bob", "[{\"name\":\"Ub\",\"height\":\"50.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output = "[{\"name\":\"Bob\",\"height\":\"50.5\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }
    @Test
    public void noSpacesBetweenLogicalOperators() {

        String[] arg = new String[]{"height>=50.5&&name=Bob", "[{\"name\":\"Bobby\",\"height\":\"55.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"},{\"name\":\"Oscar\",\"height\":\"51\"}]"};
        String output = "[{\"name\":\"Bobby\",\"height\":\"55.0\"},{\"name\":\"Bob\",\"height\":\"50.5\"}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }
    @Test
    public void andPredicatesTest() {

        String[] arg = new String[]{"name=Amaz && age>40 && height=182",
                "[{\"name\":\"Amaz\",\"height\":\"182\",\"age\":40}," +
                "{\"name\":\"Bob The Amazon Warrior Dilan\",\"height\":\"182\",\"age\":65}," +
                "{\"name\":\"Bob The Amazon Warrior Dilan\",\"height\":\"183\",\"age\":41}," +
                "{\"name\":\"Bob The Amaaon Warrior Dilan\",\"height\":\"182\",\"age\":41}]"};
        String output = "[{\"name\":\"Bob The Amazon Warrior Dilan\",\"height\":\"182\",\"age\":65}]";
        JsonFinder finder = new JsonFinder(arg[0], arg[1]);
        assertEquals(finder.checkForPresence(), output);
    }
}